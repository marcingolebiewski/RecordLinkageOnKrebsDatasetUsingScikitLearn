# -*- coding: utf-8 -*-
"""
Created on Tue May 15 11:51:46 2018

@author: marcingo
"""

import pandas as pd

#load Krebs dataset

krebsData = pd.read_csv(r'C:\RecordLinkageOnKrebsDatasetUsingScikitLearn\KrebsDataset\block_1.csv',
                        low_memory=False,
                        encoding='utf-8')

for i in range (2,11):
    temp = pd.read_csv(r'C:\RecordLinkageOnKrebsDatasetUsingScikitLearn\KrebsDataset\block_%d.csv' % i ,
                       low_memory=False,
                       encoding='utf-8')
    
    krebsData = krebsData.append(temp)
        
krebsData.replace(to_replace='?', value=0, inplace=True)
columnNames = krebsData.columns.values.tolist()
excluded_from_predictors = ['id_1', 'id_2', 'is_match']

for name in excluded_from_predictors:
    columnNames.remove(name)

unstandardizedFeatures = krebsData[columnNames]
features = unstandardizedFeatures.copy()
labels = krebsData['is_match']

# standardize features to have mean=0 and sd=1

from sklearn import preprocessing

for column in features.columns:
        features[column]=preprocessing.scale(features[column].astype('float64'))

from sklearn.model_selection import train_test_split

# split data into train and test sets
features_train, features_test, labels_train, labels_test = train_test_split(features, labels, test_size=.8, random_state=31)

#initialize and train SGDClassifier

from sklearn import linear_model

recordLinkageClassifier = linear_model.SGDClassifier(alpha=0.0001,
                                                     fit_intercept=True,
                                                     learning_rate='optimal',
                                                     loss='log',
                                                     penalty='l2',
                                                     tol=None,
                                                     max_iter=1000,
                                                     shuffle=False).fit(features_train, labels_train)

for i in range(len(features.columns)):
    print(features.columns[i], recordLinkageClassifier.coef_[0][i])
    

from sklearn.metrics import confusion_matrix

print(confusion_matrix(labels_test, recordLinkageClassifier.predict(features_test)))
